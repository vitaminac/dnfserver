./stop.sh

ln -s /vagrant/Script.pvf $DNF_HOME/neople/game/Script.pvf
ln -s /vagrant/publickey.pem $DNF_HOME/neople/game/publickey.pem

cd $DNF_HOME/neople

./ctrl.sh manager start
echo "================================Manager 	Srv Start"

sleep 1
./ctrl.sh bridge start
echo "================================Bridge 	Srv Start"

sleep 1
./ctrl.sh channel start
echo "================================Channel 	Srv Start"

sleep 1
./ctrl.sh relay start
echo "================================Relay 	Srv Start"

sleep 1
./ctrl.sh stun start
echo "================================Stun 		Srv Start"

sleep 1
./ctrl.sh coserver start
echo "===============================Coserver 	Srv Start"

sleep 1
./ctrl.sh community start
echo "===============================Community 	Srv Start"

sleep 1
./ctrl.sh all_auction start
echo "================================Auction 	Srv Start"

sleep 1
./ctrl.sh all_point start
echo "================================Point 	Srv Start"

sleep 1
./ctrl.sh all_app start
echo "================================App 		Srv Start"

cd $DNF_HOME/neople/secsvr/run/gunnersvr
./gunnersvr -t30 -i1 &

cd $DNF_HOME/neople/secsvr/run/secagent
./secagent &
./zergsvr -t30 -i1 &

sleep 1
#./ctrl.sh tp start
echo "================================TP 		Srv Start"

sleep 1
cd $DNF_HOME/neople
./ctrl.sh game start &
echo "================================Game 		Srv Start"