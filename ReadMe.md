## 生成密钥

我们需要生成一个公钥和私钥, 公钥放在服务端, 私钥用在客户端. 在当前目录用CMD或者BASH运行下面的指令, 会自动生成`privatekey.pem`和`publickey.pem`文件, 我们之后会用到

    ssh-keygen -t rsa -b 2048 -m pkcs8 -f "privatekey.pem" -q -N ""
    openssl rsa -in "privatekey.pem" -pubout -out "publickey.pem"

在Windows上运行`openssl`命令需要先安装[OpenSSL](https://community.chocolatey.org/packages/openssl).

## 配置PVF文件

下载[`Script.pvf`](https://gitlab.com/vitaminac/dnfpvf/-/blob/main/Script.pvf)然后放到根文件夹,就是这个`ReadMe`的文件夹

## 配置服务器

安装[VirtualBox](https://www.virtualbox.org/wiki/Downloads)和[Vagrant](https://developer.hashicorp.com/vagrant/downloads)然后运行

    vagrant up

## 启动服务器

虚拟机创建好了之后运行下面指令启动DNF服务器

    vagrant ssh
    cd $DNF_HOME
    ./run.sh

不用的时候直接运行下面的指令保存状态

    vagrant suspend

这样子下次可以直接加载虚拟机状态不用重新跑五国

    vagrant resume

## 错误排查

### 没有频道

如果出现没有频道,虚拟机输入两次`./stop`再输入`./run`等跑出五国

### 排除服务器启动错误

运行下面指令查看具体是哪个程序启动失败

    ps aux | grep df_

### 内存不足

运行下面指令查看是否内存不足

    free -h

如果是内存不足引起的错误可以尝试`./stop.sh`然后重新启动

## 服务器代码的调试

单一的服务器程序`df_xxx_r`可以直接丢掉IDA Pro里面转C代码看, 使用错误信息的字符串常量定位具体的代码在哪个函数里可以帮助我们理解问题. 知道是哪个函数出问题之后我们可以用`gdb`动态调试例如`gdb --args df_xxx_r xxx start`. 遇到多线程环境例如`fork`我们可以运行`set follow-fork-mode child`选择性跟踪子线程. 另一个方法是查看是否全部的端口都处于活动状态`sudo netstat -tulpn | grep df_`.

## 数据库调试

    sudo mysql -u game -h 192.168.50.10 -p'uu5!^%jg'

## DNF台服服务器代码来源

* [地下城与勇士容器版本](https://github.com/1995chen/dnf)
* [台服DNF服务端](https://github.com/skypub/DNF-server)
* [dof纯端docker一键部署](https://github.com/nnn149/dofServer)

## DNF台服架构参考

* [DNF台服原版纯净服务端带数据库](https://bbs.aladedalu.com/thread-5589-1-1.html)
* [Linux大神解读DNF单机提取端并架设外网答疑常见问题](https://tieba.baidu.com/p/4527376742)
* [DNF三职业三觉: 黑岩2020单机1.6 (更新架设，安装问题和服务端及100级补丁)](https://www.iopq.net/thread-17104910-1-1.html)
* [搭建DNF台服系列博客](https://blog.csdn.net/pmcasp/article/details/104339380)
* [Vagrant: Multi-Machine](https://www.vagrantup.com/docs/multi-machine)
* [VM版本DNF单机+开服架设教学](https://tieba.baidu.com/p/4392204923?red_tag=2786842817)
* [DNF端口说明：常见udp+tcp端口代表频道作用](https://bbs.aladedalu.com/thread-3275-1-1.html)
* [DNF台服官方操作手册](https://bbs.aladedalu.com/thread-1772-1-1.html)
* [台服DNF吧官方资源目录](https://shimo.im/docs/KjvjgRh88dCwCWDY)
* [百度贴吧 - 神牛开源计划](https://tieba.baidu.com/home/main?un=%E7%A5%9E%E7%89%9B%E9%A3%8E%E8%B7%AF%E5%88%B7%E6%96%B0%E5%A4%A7)
* [Bilibili - 神牛开源计划](https://space.bilibili.com/591113000/article)
* [DNF如何修改服务端和数据库默认的game用户与密码教程](https://bbs.aladedalu.com/thread-3117-1-1.html)
* [DNF台服数据库用户与密码修改教程](https://bbs.aladedalu.com/thread-3169-1-1.html)
* [如何通过台服流出的服务端自己架设DNF](https://www.cnblogs.com/wuter/p/14186146.html)
* [关于用 Docker 免费搭建 DNF 台服这件事](https://post.smzdm.com/p/a7d3vrp9/)
* [台服DNF手动架设服务端教程](https://bbs.aladedalu.com/thread-1210-1-1.html)
* [地下城与勇士](https://blog.marstau.com/2015/06/02/%E5%9C%B0%E4%B8%8B%E5%9F%8E%E4%B8%8E%E5%8B%87%E5%A3%AB.html)
* [dnf台服单机拍卖行、金币寄售行修复教程](https://bbs.aladedalu.com/thread-2597-1-1.html)