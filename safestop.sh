cd $DNF_HOME/neople
killall -9 df_game_r
killall -9 gunnersvr
killall -9 zergsvr
killall -9 secagent
sleep 1
./ctrl.sh all_app stop
sleep 1
./ctrl.sh all_point stop
sleep 1
./ctrl.sh all_auction stop
sleep 1
./ctrl.sh all_common stop
sleep 1
./ctrl.sh stun stop
sleep 1
./ctrl.sh relay stop
sleep 1
./ctrl.sh channel stop
sleep 3
rm -rf ./neople/game/log/*
rm -rf ./neople/game/pid/*
find . -name '*.pid' -type f -print -exec rm -rf {} \;
find . -name '*.log' -type f -print -exec rm -rf {} \;