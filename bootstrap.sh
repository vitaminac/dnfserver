#!/usr/bin/env bash

# Define Constants
export DNF_HOME=/vagrant
echo -e "\nexport DNF_HOME=$DNF_HOME" >> /home/vagrant/.bash_profile
export SWAP_FILE=/swapfile
export MYSQL_PASSWORD="uu5!^%jg"

# Install Dependencies
sudo yum check-update
sudo yum -y install \
    glibc-devel.i686 \
    GeoIP-devel.i686 \
    libstdc++-devel.i686 \
    zlib-devel.i686 \
    psmisc \
    wget \
    screen

wget -O $DNF_HOME/mysql57.rpm https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm
sudo rpm -ivh $DNF_HOME/mysql57.rpm
sudo wget -O /etc/pki/rpm-gpg/RPM-GPG-KEY-mysql https://repo.mysql.com/RPM-GPG-KEY-mysql-2022
sudo yum -y install mysql-server

# Configure MySQL
sudo sed -i '$a\skip-grant-tables=1' /etc/my.cnf
sudo systemctl start mysqld
sudo mysql -u root --execute="flush privileges; ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password'; CREATE USER 'game'@'localhost' IDENTIFIED WITH mysql_native_password BY '$MYSQL_PASSWORD'; GRANT ALL PRIVILEGES ON *.* TO 'game'@'localhost' IDENTIFIED WITH mysql_native_password BY '$MYSQL_PASSWORD' WITH GRANT OPTION; GRANT ALL PRIVILEGES ON *.* TO 'game'@'192.168.50.0/255.255.255.0' IDENTIFIED WITH mysql_native_password BY '$MYSQL_PASSWORD' WITH GRANT OPTION;"
sudo cp -f $DNF_HOME/my.cnf /etc/my.cnf
sudo systemctl restart mysqld

# Configure Server
sudo cp $DNF_HOME/neople/game/libnxencryption.so /usr/lib/
sudo chmod +x /usr/lib/libnxencryption.so

# Import Database
sudo sh -c "cd $DNF_HOME/sql; chmod +x ./restoredb.sh; ./restoredb.sh"

# Add Permissions
sudo chmod +x $DNF_HOME/run.sh
sudo chmod +x $DNF_HOME/stop.sh
sudo chmod +x $DNF_HOME/safestop.sh
sudo chown -R vagrant $DNF_HOME
sudo chmod -R +xrw $DNF_HOME

# Configure Network
sudo systemctl disable firewalld
sudo systemctl stop firewalld
sudo systemctl disable firewalld.service
sudo systemctl stop firewalld.service

# Configure Swap
sudo swapoff $SWAP_FILE
sudo dd if=/dev/zero of=$SWAP_FILE bs=1024 count=8M
sudo mkswap $SWAP_FILE
sudo swapon $SWAP_FILE

# Clean Up
rm $DNF_HOME/mysql57.rpm

# Debugging Purpose
sudo yum -y install gdb
sudo sed -i "s/enabled=0/enabled=1/g" /etc/yum.repos.d/CentOS-Debuginfo.repo
sudo yum -y install glibc-debuginfo gcc-debuginfo zlib-debuginfo
sudo debuginfo-install glibc.i686 libgcc.i686 libstdc++.i686 zlib.i686
