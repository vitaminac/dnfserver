#!/usr/bin/env bash

base_dir=$(cd "$(dirname "$0")"; pwd)
tmp_path="/tmp"
dev_shm="/dev/shm"
secagent_zerg_path="$base_dir/../run/secagent"
gunnersvr_zerg_path="$base_dir/../run/gunnersvr"

# sdk <-> secagent channel data
# delete old
ls -l ${tmp_path}/tss_sdk_bus_* >/dev/null 2>&1
rm -f ${tmp_path}/tss_sdk_bus_* >/dev/null 2>&1 
# delete new
ls -l ${dev_shm}/sec/tss_sdk_bus_* >/dev/null 2>&1
rm -f ${dev_shm}/sec/tss_sdk_bus_* >/dev/null 2>&1 

# gunner monitor data
ls -l ${dev_shm}/*.STAT >/dev/null 2>&1
rm -f ${dev_shm}/*.STAT >/dev/null 2>&1

# dp config
ls -l ${tmp_path}/dp_* >/dev/null 2>&1
rm -f ${tmp_path}/dp_* >/dev/null 2>&1

# zerg data
ls -l ${secagent_zerg_path}/*.MMAP >/dev/null 2>&1
rm -f ${secagent_zerg_path}/*.MMAP >/dev/null 2>&1
ls -l ${gunnersvr_zerg_path}/*.MMAP >/dev/null 2>&1
rm -f ${gunnersvr_zerg_path}/*.MMAP >/dev/null 2>&1

# dp shm
ls -l ${tmp_path}/tss_sdk_shm_* >/dev/null 2>&1
rm -f ${tmp_path}/tss_sdk_shm_* >/dev/null 2>&1
