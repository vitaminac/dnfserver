#!/usr/bin/env bash

# 脚本使用提示
function usage()
{
	echo
	echo " usage: tss_svr.sh {svrd} [arg] {start|stop|stat|safe_stop|reload}"
	echo " arg: -p, pull config from cfgsvr"
	echo " arg: -a, cfgsvr ip"
	echo " arg: -t, svrd type"
	echo " arg: -i, svrd index"
	echo " eg: tss_svr.sh zergsvr -p -a {your server ip} -t 2-t 2 -i 1 start"
	echo " if you wantn't to monitoritor your programe. you need use:"
	echo " tss_svr.sh svrd start nomonitoritor. default is monitoritor."
	echo
}

# 获取进程的pid
function get_pid()
{
	if [ -f $svc_pid ]; then
		read proc_id < $svc_pid
	else
		proc_id=0
	fi

	if [ -f $monitor_pid ]; then
		read safe_id < $monitor_pid
	else
		safe_id=0
	fi
}

# 由进程pid获取进程的svr_type和svr_id
# 必须在get_pid之后调用
function get_svrinfo()
{
	echo "get_svrinfo pid=$proc_id "
	shm_file=`/usr/sbin/lsof -p $proc_id-X| grep .STAT | head -1 | awk '{print $9}'`
	echo "get_svrinfo shm_file=$shm_file"
	tmp_val=${shm_file##*_}
	svr_id=${tmp_val%%.*}

	tmp_val=${shm_file#*_}
	svr_type=${tmp_val%%_*}
}

# 告警上报监控服务器
function report_to_monitor()
{
	# 每隔5分钟上报一次,在准点5分钟之后的0-10秒内上报
	current_report_time=`date +%s`
	FIVE_MINUTES=300

	let right_time=$current_report_time%$FIVE_MINUTES
	if [ $right_time -le 15 ];then

		let interval=$current_report_time-$last_report_time
		# 保证上报间隔在5分钟以上
		if [ $interval -ge $FIVE_MINUTES ];then
			feature_value=$proc_reboot_times

			# 发送数据给Monitor
			gunner_name=`ls .. | grep gunner`
			cd ../$gunner_name
			../../bin/gunnersvr --as-tool -r -t $svr_type -i $svr_id -q $feature_id -e $feature_value >/dev/null 2>&1
			cd - >/dev/null

			# 更新下状态
			last_report_time=$current_report_time
			proc_reboot_times=0
		fi
	fi
}

# 对服务器进程监控，每10秒查看一次进程是否被其他方式退出，如果已退出重新启动
function monitor()
{
	last_report_time=0
	current_report_time=0

	while true; do
		sleep 10
		get_pid
		report_to_monitor

		if test "$proc_id" == "0"; then
			echo "$(date) $svc_bin exit, safe down" | tee -a $svc_log
			exit 3
		else
			kill -0 $proc_id > /dev/null 2>&1
			
			if test "$?" != "0"; then
				echo "$(date) $svc_bin has gone, will restart" | tee -a $svc_log
				let proc_reboot_times=$proc_reboot_times+1
				
				# 重启zergsvr
				boot_zergsvr
			fi
		fi
done
}

# 启动zergsvr
function boot_zergsvr()
{
	# 获得core的pid
	core_path="/data/corefile"
	cd $core_path
	core_pid=`ls -l | grep core_zergsvr | awk '{print $NF}' | awk -F'.' '{print $2}' `
	cd - > /dev/null 2>&1

	same_num=`echo $core_pid | awk -v pid=$proc_id '{for(i=1;i<= NF;i++){ if($i==pid) { print $i } } }' | wc -l`
	# 判断是否core了
	if test "$same_num" == "1"; then
		if [ -f ZERGPIPE.31.*.MMAP ];then
			# 先备份共享内存数据
			mv ZERGPIPE.31.*.MMAP ~ZERGPIPE.MMAP.bak
		fi
	
		# kill secsvr
		read secsvr_pid < secsvr.pid 
		kill -9 $secsvr_pid
	fi

	# 最后拉起zergsvr
	boot
}

# 启动服务器进程
function boot()
{
	echo "$svc_cmd"
	svc_cmd1="../../bin/$svc_cmd"
	echo "$svc_cmd1"
	$svc_cmd
	echo "boot execute finish"

	if test "$?" != "0"; then
		echo "$(date) $svc_bin start failed" | tee -a $svc_log
		exit 1
	else
		echo "test"
		sleep 2
		get_pid
		echo "get pid finish"
		get_svrinfo
		echo "get svn info "
		
		echo "$(date) $svc_bin started, pid: $proc_id" | tee -a $svc_log
		# 写监控进程pid到文件
		echo $! > $monitor_pid
	fi
}

# 执行start命令，先启动进程，然后启动监控进程进行监控
function start()
{
	get_pid

	if test "$proc_id" == "0"; then
	echo "start boot"
		boot
	echo "start monitor"
		monitor &
		# 获取子进程的pid
		echo $! > $monitor_pid
	else
		kill -0 $proc_id > /dev/null 2>&1
		
		if test "$?" != "0"; then
			boot
			monitor &
			# 获取子进程的pid
			echo $! > $monitor_pid
		else
			echo "$(date) $svc_bin already running, pid: $proc_id"
		fi
	fi
}

# 执行stop命令，停止监控进程和服务器进程，并不保证服务器进程一定退出
function stop()
{
	get_pid

	# 为了安全，先停监控脚本
	if test "$safe_id" == "0"; then
		echo "$(date) monitoritor not running"
	else
		kill -9 $safe_id
		
		echo "$(date) kill -9 $safe_id"
		echo "$(date) monitoritor stop"
		rm -f $monitor_pid > /dev/null 2>&1
	fi

	# 然后才停监控进程
	if test "$proc_id" == "0"; then
		echo "$(date) $svc_bin not running"
	else
		kill -2 $proc_id > /dev/null 2>&1

		echo "$(date) kill -2 $proc_id" | tee -a $svc_log
		echo "$(date) $svc_bin stop" | tee -a $svc_log
		rm -f $svc_pid > /dev/null 2>&1
	fi
}

# 执行safestop命令，停止监控进程和服务器进程，直到确认服务器进程退出才退出
function safe_stop()
{
	get_pid

	#先停止监控脚本
	if test "$safe_id" == "0"; then
		echo "$(date) monitoritor not running"
	else
		kill -9 $safe_id
		
		echo "$(date) kill -9 $safe_id"
		rm -f $monitor_pid > /dev/null 2>&1
	fi

	#后停止监控进程
	if test "$proc_id" == "0"; then
		echo "$(date) $svc_bin not running"
	else
		echo "$(date) kill -3 $proc_id"
		kill -3 $proc_id > /dev/null 2>&1
		
		#检查程序是否退出
		while true; do
			sleep 1
			kill -0 $proc_id > /dev/null 2>&1
			
			if test "$?" != "0"; then
				echo "$(date) $svc_bin has gone, safe stop finish" | tee -a $svc_log
				break;
			fi
		done
		
		rm -f $svc_pid > /dev/null 2>&1
	fi
}

# 执行stat命令，查看服务器进程的状态
function stat()
{
	get_pid
	
	if test "$proc_id" == "0"; then
		echo "$(date) $svc_bin not running"
	else
		kill -0 $proc_id > /dev/null 2>&1
		
		if test "$?" != "0"; then
			echo "$(date) $svc_bin not running"
		else
			echo "$(date) $svc_bin ok, pid: $proc_id"
		fi
	fi
}

# 执行stat命令，查看服务器进程的状态
function reload()
{
	get_pid
	
	if test "$proc_id" == "0"; then
		echo "$(date) $svc_bin not running"
	else
		kill -10 $proc_id > /dev/null 2>&1
		
		if test "$?" != "0"; then
			echo "$(date) $svc_bin not running"
		else
			echo "$(date) $svc_bin reload, pid: $proc_id"
		fi
	fi
}

# 脚本主函数
function main()
{
	ulimit -c unlimited
	ulimit -n 204800
	export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
	export PATH=/data/gamesafe/bin:$PATH

	# 忽略掉HUP,QUIT和TERM信号 
	trap '' 1 3 15

	if [ "$#" -lt "2" ]; then
		usage
		exit 1
	fi
	last_argc=""
	# 获取服务器的执行命令, svr arg arg...
	let argc=0
	for argv in $@; do
		last_argc=$argv
		let argc=$argc+1
		if [ $argc -eq $# ]; then
			break
		fi
		svc_cmd="$svc_cmd"" ""$argv"
	done

	#1svc_cmd="$svc_cmd"" -d -a 203.69.93.200"
	#svc_cmd="$svc_cmd"" -d "
	svc_cmd="$svc_cmd"" -d -a 127.0.0.1"
	svc_exec=$1
	svc_bin="$svc_exec"
	svc_pid="$svc_bin".pid
	svc_log=tss_svr."$svc_exec".log
	monitor_pid=tss_svr."$svc_exec".pid

	# 指示进程是否启动
	proc_reboot_times=0

	# 监控服务器状态的相关数据
	feature_id=9200
	feature_value=0
	svr_type=0
	svr_id=0
	
	if test $last_argc == "start"; then
		ulimit -c unlimited
		start
	elif test $last_argc == "stop"; then
		stop
	elif test $last_argc == "safe_stop"; then
		safe_stop
	elif test $last_argc == "stat"; then
		stat
	elif test $last_argc == "reload"; then
		reload
	else
		usage
	fi

	exit 0
}

main $@