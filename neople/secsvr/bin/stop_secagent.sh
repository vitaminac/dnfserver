#!/usr/bin/env bash

base_dir=$(cd "$(dirname "$0")"; pwd)
CUR_DIR=`pwd`

SECAGENT_RUN_DIR="${base_dir}/../run/secagent"
GUNNERSVR_RUN_DIR="${base_dir}/../run/gunnersvr"
TSS_SVR_SH="../../bin/tss_svr.sh"

export PATH=$PATH:${base_dir}

# secagent
cd ${SECAGENT_RUN_DIR}
sh ${TSS_SVR_SH} zergsvr -p -t 30 -i 1 stop
sh ${TSS_SVR_SH} secagent -p -i 1 stop

# gunnersvr
cd ${GUNNERSVR_RUN_DIR}
sh ${TSS_SVR_SH} gunnersvr -p -t 30 -i 1 stop